let registerForm = document.querySelector("#registerUser")


//a user interaction such as loading the page, clicking on an HTML element, typing something, or submitting a form is called an event

//since we dont want the default behavior of our form submission event, we can do that

//by using addEventListener to bind our code to the form submission event, we can do that

registerForm.addEventListener("submit", (e) => {
	// e in the paramaters refers to the event itself (e is a placeholder name and can be anything)
	e.preventDefault()// prevents our form from reloading the page wehn submitted



let firstName = document.querySelector("#firstName").value
let lastName = document.querySelector("#lastName").value
let mobileNo = document.querySelector("#mobileNumber").value
let email = document.querySelector("#userEmail").value
let password1 = document.querySelector("#password1").value
let password2 = document.querySelector("#password2").value


//validation to enable form submission only when all fields are populated, when passwords matched, and when mobile number is exactly 11 numbers long

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)){
		
		// alert(`User ${firstName} ${lastName} with email ${email} and mobile number ${mobileNo} succesfully registered!`)
		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email

			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(`It is ${data} that there is a duplicate email`)
			// console.log(`${data}`)

			if(data){
				alert("Duplicate email found, please use another email address")
			}else{
				//create a fetch request in this else statement that allows our user to register
				//Properly handle the response so that a successful registration shows an alert that say registration successful, an alert that says registration is NOT successful if NOt
				fetch('http://localhost:4000/users/register',{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password1: password1,
						password2: password2
					})
					
				})
				.then(res=> res.json())
				.then(data => {
					console.log ("Successfully Registered!")
				})
			}

		})

	}else{
		alert("Please check your registration details and try again")
	}

})